import axios from 'axios';
import axiosRetry from 'axios-retry';

const healthAxios = axios.create()

axiosRetry(axios, {
  retries: 5,
  retryDelay: (...arg) => axiosRetry.exponentialDelay(...arg, 1000),
  retryCondition(error) {
    if(!(error?.response?.status)) {
      return true;
    }
    switch ((error?.response?.status)) {
      //retry only if status is 500 or 501
      case 500:
      case 501:
        return true;
      default:
        return false;
    }
  },
  onRetry: async (retryCount, error, requestConfig) => {
    console.log(`retry count: ${requestConfig.url}`, retryCount);
    if (retryCount > 2) {
      const podURL = requestConfig.url?.split('/')[2]
      console.log('checking pod status with URL: ', podURL);
      const podHealthy = await healthAxios.get(`http://${podURL}/health`)
        .then(res => res.status === 200)
        .catch(_ => false);
      const retryConfig = requestConfig!['axios-retry']
      if(!podHealthy && retryConfig){
        console.log(`pod with URL: ${podURL} are down!!!`);
        throw(error);
      }
    }

  },
});

export const pushMsg = (baseUrl: string) => async (msg: any): Promise<boolean> => {
  console.log(`running pushMsg to ${baseUrl} for msg with id: ${msg.id}`)
  try {
    const res = await axios.post(
      `${baseUrl}/add`,
      msg,
      {
        headers: {
          Accept: 'application/json',
        },
      },
    );
    console.log(`push to ${baseUrl} for msg with id: ${msg.id} done`, JSON.stringify(res.data, null, 4))
    if (res.status == 201) {
      return true
    } else {
      return false
    }
  } catch (error) {
    if (axios.isAxiosError(error)) {
      console.log('error message: ', error.message);
      return false;
    } else {
      console.log('unexpected error: ', error);
      return false;
    }
  }
}

export const askMsg = (baseUrl: string) => async (id: string): Promise<boolean> => {
  console.log(`running ask to ${baseUrl} with id: ${id}`)
  try {
    const res = await axios.get(
      `${baseUrl}/ask/${id}`,
      {
        headers: {
          Accept: 'application/json',
        },
      },
    );

    console.log(`Done ask to ${baseUrl} for msg with id: ${id}`, JSON.stringify(res.data, null, 4))

    if (res.status == 200) {
      return true
    } else {
      return false
    }
  } catch (error) {
    if (axios.isAxiosError(error)) {
      console.log('error message: ', error.message);
      return false;
    } else {
      console.log('unexpected error: ', error);
      return false;
    }
  }
}

export const sync = async (baseUrl: string): Promise<any[] | null> => {
  try {
    const res = await axios.get(
      `${baseUrl}/`,
      {
        headers: {
          Accept: 'application/json',
        },
      },
    );

    console.log(JSON.stringify(res.data, null, 4));

    if (res.status == 200) {
      return res.data
    } else {
      return null
    }
  } catch (error) {
    if (axios.isAxiosError(error)) {
      console.log('error message: ', error.message);
      return null;
    } else {
      console.log('unexpected error: ', error);
      return null;
    }
  }
}