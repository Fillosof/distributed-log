const port = process.env.PORT || 8080;
const main = !!(+(process.env.IS_MAIN|| 1));
const randomErrors = !!(+(process.env.RANDOM_ERROR|| 1));
const pod1Port = process.env.POD1_PORT || 8081;
const pod2Port = process.env.POD2_PORT || 8082;
const mainPort = process.env.MAIN_PORT || 8080;
const sleep = +(process.env.SLEEP || '0');

export default {
  port,
  main,
  pod1Port,
  pod2Port,
  mainPort,
  sleep,
  randomErrors,
}