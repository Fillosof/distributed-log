import express, { Request, Response } from 'express';
import config from './config'
import { pushMsg, askMsg, sync } from './axios'

const app = express();
app.use(express.json());

function delay(ms: number) {
  return new Promise((resolve) => {
    setTimeout(resolve, ms);
  });
}
const getRand = () => Math.random() < 0.5

const syncPod = (url: string) => async (msg: any) => {
  await pushMsg(url)(msg);
  await askMsg(url)(msg.id)
}
const syncP1 = syncPod(`http://172.17.0.1:${config.pod1Port}`)
const syncP2 = syncPod(`http://172.17.0.1:${config.pod2Port}`)

let latestId = 1;

interface Msg {
  id: number,
  msg: string,
  w: number,
}

const msg: Record<string, Msg> = {
  'key_1': {
    id: 1,
    msg: 'init message',
    w: 1,
  }
}

// list all messages
app.get('/', async (req: Request, res: Response) => {
  if (config.sleep) {
    await delay(config.sleep);
  }
  const syncedMsg = Object.values(msg).filter((message) => message.id <= latestId)
  res.status(200).json(Object.values(syncedMsg));
});

// Add new message
app.post('/', async (req: Request, res: Response) => {
  if (!config.main) {
    return res.status(401).send('Cannot create new message it`s secondary').end()
  }
  const message = req.body;
  latestId++
  const newMsg = {
    id: latestId,
    msg: message.msg,
    w: message.w || 1
  }
  msg[`key_${latestId}`] = newMsg
  if (message.w === 1) {
    syncP1(newMsg);
    syncP2(newMsg);
    console.log('Get quorum for w=1')
  } else if (message.w === 2) {
    await Promise.any([
      syncP1(newMsg),
      syncP2(newMsg)
    ])
    console.log('Get quorum for w=2')
  } else if (message.w === 3) {
    await Promise.all([
      syncP1(newMsg),
      syncP2(newMsg)
    ])
    console.log('Get quorum for w=3')
  }
  res.status(201).json(Object.values(msg));
});

// Push message to secondary
app.post('/add', async (req: Request, res: Response) => {
  if (config.main) {
    return res.status(401).send('Cannot push message it`s main').end()
  }
  if (config.sleep) {
    await delay(config.sleep);
  }
  const message = req.body;
  if (config.randomErrors && getRand()) {
    console.log('Error strikes on push for id:', message.id)
    return res.status(501).send('Waaaahhh Error').end()
  }
  latestId++
  msg[`key_${message.id}`] = {
    id: message.id,
    msg: message.msg,
    w: message.w
  }
  res.status(201).json(Object.values(msg));
});

// Verify message
app.get('/ask/:id', async (req: Request, res: Response) => {
  console.log('config.randomErrors', config.randomErrors)
  const id = req.params.id;
  console.log('getting ask for id:', id)
  if (config.main) {
    return res.status(401).send('Cannot verify message it`s main').end()
  }
  if (config.randomErrors && getRand()) {
    console.log('Error strikes on ask for id:', id)
    return res.status(501).send('Waaaahhh Error').end()
  }
  latestId++
  const message = msg[`key_${id}`]
  if (message) {
    console.log('ask done for msg:', msg[`key_${id}`])
    res.json(message).status(200);
  } else {
    console.log('ask fails for id:', id)
    res.json({}).status(404);
  }
});

app.get('/health', async (req: Request, res: Response) => {
  res.json('').status(200);
});



const syncToMater = async () => {
  if (!config.main) {
    console.log('Running secondary node');
    console.log('Syncing ...');
    try {
      const mainData = await sync(`http://172.17.0.1:${config.mainPort}`)
      if(mainData && mainData.length) {
        mainData.forEach((data) => {
          msg[`key_${data.id}`] = data
          if (latestId < data.id) {
            latestId = data.id
          }
        })
        console.log('msg:', msg)
        console.log('latestId:', latestId)
      }
      console.log('Syncing successful');
    } catch (error) {
      console.log('Syncing fail error:', error);
    }
  }
}

app.listen(config.port, () => {
  syncToMater();
  console.log(`Server running at http://localhost:${config.port}`);
});
